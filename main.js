    

    searchWordsList.style.visibility = 'hidden';    //hide before choose a input file.

    //event to select a word to search.
    searchWordsList.addEventListener('change', function() {
        found = false;
        findWord(searchWordsList.value);
    })

    //event to read input file and populate search strings array.
    document.getElementById('alphabetFile').addEventListener('change', function() {
       (new alphabetUtils).resetElements(searchWordsList, matrix, table, output);
        readFile(this.files[0]);
    }); 

    //read and show Alphabet data, populate Alphabet matrix and get search words.
    function readFile(dataFile) {
        let fr = new FileReader();
        fr.onload = function() {
            let data = fr.result;
            utils = new alphabetUtils();
            let alphaTable = new alphabetTable("alphabetTable", 0, 0);

            //show input data.
            document.getElementById('alphabetData').textContent = fr.result;

            //remove any falsy elements such as "", false, NaN and "undefined".
            rawDataArray = data.split('\r\n').filter(elm => elm);    

            //get rows/columns length.
            let rowsCols = rawDataArray[0].split('x');
            let rows = parseInt(rowsCols[0]);
            let cols = parseInt(rowsCols[1]);
            rawDataArray.shift();  //remove first element, like "5x5".

            //get matrix and search words.
            rawDataArray.filter(function(el) {
                //if first two characters are alphabetic, then the element is search word, otherwise is matrix element.
                if (/^[a-zA-Z]+$/.test(el.substr(0, 2))) {  
                    searchWords.push(el);
                } else {
                    alphabetMatrix.push(el.replace(/\s/g, ''));
                }
            }) 

            //build alphabet matrix for found word to be highlighted.
            alphaTable.rows = rows;
            alphaTable.cols = cols;
            table = alphaTable.buildRowsCols(alphabetMatrix);
            matrix.appendChild(table);

            //populate search word to dropdown list.
            searchWordsList.innerHTML = "";
            searchWordsList.style.visibility = "visible";

            //populate the search word list.
            utils.populateSearchWords(searchWords, searchWordsList);

            buildSearchMatrix();
        }   
    
        fr.readAsText(dataFile);
    }

    //search word.
    function findWord(searchWord) {
        let foundPos = "", wordRange = [], range = "", utils = new alphabetUtils();
        let filtered = alphabetSoupData.stringArray.filter(function(el) {
            if (el.searchString.indexOf(searchWord) >= 0) { //forward search
                return el;
            } else {    //reverse search.
                let rEl = el.searchString.split('').reverse().join('');
                if (rEl.indexOf(searchWord) >= 0) {
                    return el;
                }
            } 
        });

        filtered.map(function(el) {
            if (el.searchString.indexOf(searchWord) >= 0) { //forward search
                foundPos = el.searchString.indexOf(searchWord);
                wordRange = el.range.split('|').filter(e => e);  //remove empty string.
                range = `${wordRange[foundPos]} ${wordRange[foundPos + searchWord.length - 1]}`;
                output.innerText = `Output: ${searchWord}   [${range}]`;
                found = true;
                utils.highlightCells(table, range);
            } else {    //reverse search.
                let rEl = el.searchString.split('').reverse().join('');
                if (rEl.indexOf(searchWord) >= 0) {
                    foundPos = rEl.indexOf(searchWord);
                    wordRange = el.range.split('|').filter(el => el).reverse();  
                    range = `${wordRange[foundPos]} ${wordRange[foundPos + searchWord.length - 1]}`;
                    utils.resetCells(table);
                    utils.highlightReverseCells(table, range);
                    output.innerText = `Output: ${searchWord}   [${range}]`;
                    found = true;
                }
            } 
        });

        if (!found) {
            utils.resetCells(table);
            output.innerText = `Output: ${searchWord}   [not found]`;
        }
    }
 
    //get strings in all rows/columns/diagonals to an array along with the strings' ranges.
    function buildSearchMatrix() {
        //rows' strings.
        let searchString = "", cellPos = "", utils = new alphabetUtils();
        for (let i = 0; i < table.rows.length; i++) {
            for (let j = 0; j < table.rows[i].cells.length; j++) {
                searchString = `${searchString}${alphabetMatrix[i].substr(j, 1)}`;
                cellPos = `${cellPos}|${i}:${j}`;
            }

            alphabetSoupData.stringArray.push({"searchString": `${searchString}`,"range": `${cellPos}`});
            searchString = "";
            cellPos = "";
        }

        //columns' strings.
        for (let i = 0; i < table.rows[0].cells.length; i++) {
            for (let j = 0; j < table.rows.length; j++) {
                searchString = `${searchString}${alphabetMatrix[j].substr(i, 1)}`;
                cellPos = `${cellPos}|${j}:${i}`;
            }

            alphabetSoupData.stringArray.push({"searchString": `${searchString}`,"range": `${cellPos}`});
            searchString = "";
            cellPos = "";
        }
        
        //diagonal strings, "/" direction.
        for (let i = 0; i < table.rows[0].cells.length; i++) {    //move along first row, first column
            utils.getDiagonalStringOnRow(`0:${i}`, searchString, cellPos, "fr");
        }

        //then moving down along last column, start from second row.
        for (let i = 1; i <= table.rows.length - 1; i++) {
            utils.getDiagonalStringOnCol(`${i}:${table.rows[0].cells.length - 1}`, searchString, cellPos, "fc");
        }

        //diagonal strings, "\" direction.
        for (let i = 0; i < table.rows[0].cells.length; i++) {    //move along last row first column.
            utils.getDiagonalStringOnRowBackSlash(`${table.rows.length - 1}:${i}`, searchString, cellPos, "br");
        }

        //then moving down along first column, start from last second row.
        for (let i = rows - 1; i > 0; i--) {
            utils.getDiagonalStringOnColBackSlash(`${i - 1}:${table.rows[0].cells.length - 1}`, searchString, cellPos, "bc");
        }

    }








