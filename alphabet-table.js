class alphabetTable {
    constructor(id, rows, cols) {
        this.id = id;
        this.rows = rows;
        this.cols = cols
    }

    buildRowsCols(data) {
        let tableDiv = document.createElement('table')
        for (let i = 0; i < this.rows; i++) {
            const tr = tableDiv.insertRow();
            for (let j = 0; j < this.cols; j++) {
                const td = tr.insertCell();
                td.textContent = data[i].substr(j, 1);
                td.style.border = '1px solid black';
                td.style.backgroundColor = '#b3f0ff';
            }
        }
        return tableDiv;
    }
  };