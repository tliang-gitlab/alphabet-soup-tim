class alphabetUtils {

    //populate the search word list.
    populateSearchWords(searchWords, searchWordsList) {
        //add placeholder to dropdown list.
        let option = document.createElement("option");
        option.text = "Please a word";
        option.value = "";
        option.disabled = "disabled";
        option.selected = "selected";
        searchWordsList.add(option);
        
        for(let i = 0; i < searchWords.length; i++) {
            let option = document.createElement("option");
            option.text = searchWords[i];
            option.value = searchWords[i];
            searchWordsList.add(option);
        }
    }

    
    //recursive function to build diagonal strings, "/" direction.
    getDiagonalStringOnRow(cell, searchString, cellPos) {
        let cellRow = parseInt(cell.split(':')[0]);
        let cellCol = parseInt(cell.split(':')[1]);

        searchString = `${searchString}${alphabetMatrix[cellRow].substr(cellCol, 1)}`;
        cellPos = `${cellPos}|${cell}`;

        if (cellCol > 0) {
            this.getDiagonalStringOnRow(`${cellRow + 1}:${cellCol - 1}`, searchString, cellPos);
        } else {
            alphabetSoupData.stringArray.push({"searchString": `${searchString}`,"range": `${cellPos}`});
            searchString = "";
            cellPos = "";                 
        }
    }

    //recursive function to build diagonal strings, "/" direction.
    getDiagonalStringOnCol(cell, searchString, cellPos) {
        let cellRow = parseInt(cell.split(':')[0]);
        let cellCol = parseInt(cell.split(':')[1]);

        searchString = `${searchString}${alphabetMatrix[cellRow].substr(cellCol, 1)}`;
        cellPos = `${cellPos}|${cell}`;

        if (cellRow < table.rows.length - 1) {
            this.getDiagonalStringOnCol(`${cellRow + 1}:${cellCol - 1}`, searchString, cellPos);
        } else {
            alphabetSoupData.stringArray.push({"searchString": `${searchString}`,"range": `${cellPos}`});
            searchString = "";
            cellPos = "";
        }
    }
    
    //recursive function to build diagonal strings, "\" direction.
    getDiagonalStringOnRowBackSlash(cell, searchString, cellPos) {
        let cellRow = parseInt(cell.split(':')[0]);
        let cellCol = parseInt(cell.split(':')[1]);

        searchString = `${searchString}${alphabetMatrix[cellRow].substr(cellCol, 1)}`;
        cellPos = `${cellPos}|${cell}`;

        if (cellCol > 0) {
            this.getDiagonalStringOnRowBackSlash(`${cellRow - 1}:${cellCol - 1}`, searchString, cellPos);
        } else {
            alphabetSoupData.stringArray.push({"searchString": `${searchString}`,"range": `${cellPos}`});
            searchString = "";
            cellPos = "";                 
        }
    }

    //recursive function to build diagonal strings, "\" direction.
    getDiagonalStringOnColBackSlash(cell, searchString, cellPos) {
        let cellRow = parseInt(cell.split(':')[0]);
        let cellCol = parseInt(cell.split(':')[1]);

        searchString = `${searchString}${alphabetMatrix[cellRow].substr(cellCol, 1)}`;
        cellPos = `${cellPos}|${cell}`;

        if (cellRow > 0) {
            this.getDiagonalStringOnColBackSlash(`${cellRow - 1}:${cellCol - 1}`, searchString, cellPos);
        } else {
            alphabetSoupData.stringArray.push({"searchString": `${searchString}`,"range": `${cellPos}`});
            searchString = "";
            cellPos = "";
        }
    }

    highlightCells(table, cellRange) {
        let startCell = cellRange.split(' ')[0];
        let startCellRow = startCell.split(':')[0];
        let startCellCol = startCell.split(':')[1];

        let endCell = cellRange.split(' ')[1];
        let endCellRow = endCell.split(':')[0];
        let endCellCol = endCell.split(':')[1];

        let utils = new alphabetUtils()
        utils.resetCells(table);

        if (startCellRow === endCellRow) { //same row
            for (let i = startCellCol; i <= endCellCol; i++) {
                let cell = table.rows[startCellRow].cells[i]
                cell.style.backgroundColor = 'red';
                cell.style.color = 'white';
            }
        } else if (startCellCol === endCellCol) {   //same column
            for (let i = startCellRow; i <= endCellRow; i++) {
                let cell = table.rows[i].cells[startCellCol]
                cell.style.backgroundColor = 'red';
                cell.style.color = 'white';
            }
        } else {    //diagonal
            utils.highlightDiagonalCells(table, cellRange);
        } 

    }

    //recursive call to highlight diagonal cells.
    highlightDiagonalCells(table, cellRange) {
        let startCell = cellRange.split(' ')[0];
        let startCellRow = parseInt(startCell.split(':')[0]);
        let startCellCol = parseInt(startCell.split(':')[1]);

        let endCell = cellRange.split(' ')[1];
        let endCellRow = endCell.split(':')[0];
        let endCellCol = endCell.split(':')[1];

        let ce = table.rows[startCellRow].cells[startCellCol];
        ce.style.backgroundColor = 'red';
        ce.style.color = 'white';

        if (startCell !== endCell) {
            switch(true) {
                case startCellRow < endCellRow && startCellCol < endCellCol:    //going down diagonally left to right.
                    this.highlightDiagonalCells(table, `${startCellRow + 1}:${startCellCol + 1} ${endCell}`);
                    break;

                case startCellRow > endCellRow && startCellCol > endCellCol:    //going up diagonally right to left. 
                    this.highlightDiagonalCells(table, `${startCellRow - 1}:${startCellCol - 1} ${endCell}`);
                    break;

                case startCellRow > endCellRow && startCellCol < endCellCol:    //going up diagonally left to right. 
                    this.highlightDiagonalCells(table, `${startCellRow - 1}:${startCellCol + 1} ${endCell}`);
                    break;

                case startCellRow < endCellRow && startCellCol > endCellCol:    //going down diagonally right to left.
                    this.highlightDiagonalCells(table, `${startCellRow + 1}:${startCellCol - 1} ${endCell}`);
                    break;
            }
        }
    }

    highlightReverseCells(table, cellRange) {
        let startCell = cellRange.split(' ')[0];
        let startCellRow = startCell.split(':')[0];
        let startCellCol = startCell.split(':')[1];

        let endCell = cellRange.split(' ')[1];
        let endCellRow = endCell.split(':')[0];
        let endCellCol = endCell.split(':')[1];

        if (startCellRow === endCellRow) { //same row
            for (let i = startCellCol; i >= endCellCol; i--) {
                let cell = table.rows[startCellRow].cells[i]
                cell.style.backgroundColor = 'red';
                cell.style.color = 'white';
            }
        } else if (startCellCol === endCellCol) {   //same column
            for (let i = startCellRow; i >= endCellRow; i--) {
                let cell = table.rows[i].cells[startCellCol]
                cell.style.backgroundColor = 'red';
                cell.style.color = 'white';
            }
        } else {    //diagonal
            this.highlightDiagonalCells(table, cellRange);
        } 
    }

    //reset all variables and element content.
    resetElements(searchWordsList, matrix, table, output) {
        rows = 0, 
        cols = 0, 
        rawDataArray = [],   
        alphabetMatrix = [], 
        searchWord = "", 
        searchWords = [],
        found = false, 
        alphabetSoupData = {stringArray: []}; 

        searchWordsList.innerHTML = '';
        matrix.innerHTML = '';
        table.innerHTML = '';
        output.innerHTML = '';
    }

    //reset cell color.
    resetCells(table) {
        for (let i = 0; i < table.rows.length; i++) {
            let row = table.rows[i];
            for (let j = 0; j < row.cells.length; j++) {
                let cell = row.cells[j];
                cell.style.backgroundColor = '#b3f0ff';
                cell.style.color = 'black';
            }
        }
    }
  };